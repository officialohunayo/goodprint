SHIRT_CHOICES = (
    ('S', 'S'),
    ('M', 'M'),
    ('L', 'L'),
    ('XL', 'XL'),
    ('XXL', 'XXL')
)

ASIZE_CHOICES = (
    ('a1', 'A1'),
    ('a2', 'A2'),
    ('a3', 'A3'),
    ('a4', 'A4')
)

BANNER_SIZE = (
    ("2.5meter", "2.5 Meter"),
    ("3.5meter", "3.5 Meter"),
    ("4.5meter", "4.5 Meter")
)

ADDRESS_CHOICES = (
    ('B', 'Billing'),
    ('S', 'Shipping'),
)
