from backend.models import Product, ProductCategory, ProductGallery, User
from django.contrib import admin

# Register your models here.
admin.site.register(User)
admin.site.register(Product)
admin.site.register(ProductCategory)
admin.site.register(ProductGallery)
