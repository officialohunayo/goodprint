from backend.specifications import ASIZE_CHOICES, BANNER_SIZE
from django.db import models
from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.conf import settings
from rest_framework.authtoken.models import Token

import datetime
from datetime import timedelta, timezone
from datetime import datetime as dt

today = datetime.date.today()


# Custom models here.
class UserManager(BaseUserManager):
	"""
	Custom user model manager where email is the unique identifiers
	for authentication instead of username.
	"""
	def create_user(self, email, password, **extra_fields):
		"""
		Create and save a User with the given email and password.
		"""
		if not email:
			raise ValueError(_('The Email must be set'))
		email = self.normalize_email(email)
		user = self.model(email=email, **extra_fields)
		user.set_password(password)
		user.save()
		return user

	def create_superuser(self, email, password, **extra_fields):
		"""
		Create and save a SuperUser with the given email and password.
		"""
		extra_fields.setdefault('is_staff', True)
		extra_fields.setdefault('is_superuser', True)
		extra_fields.setdefault('is_active', True)

		if extra_fields.get('is_staff') is not True:
			raise ValueError(_('Superuser must have is_staff=True.'))
		if extra_fields.get('is_superuser') is not True:
			raise ValueError(_('Superuser must have is_superuser=True.'))
		return self.create_user(email, password, **extra_fields)


#### This is User Profile
class User(AbstractUser):
	user_gender = (
		('Male', 'Male'),
		('Female', 'Female')
	)
	username = None
	first_name = models.CharField(_("First Name"), max_length=200, null=True)
	last_name = models.CharField(_("Last Name"), max_length=200, null=True)
	email = models.EmailField(_('Email Address'), default='')
	gender = models.CharField(max_length=10, default='', choices=user_gender)
	mobile = models.CharField(max_length=200, unique=True)
	profile_image = models.ImageField(upload_to='users', default="/static/img/profile1.png", null=True)
	state = models.CharField(max_length=100, null=True)
	address = models.TextField(default="", null=True)

	USERNAME_FIELD = 'mobile'
	REQUIRED_FIELDS = ['first_name', 'last_name', 'email']

	objects = UserManager()

	def __str__(self):
		return self.first_name + ' - ' + self.last_name

@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
	if created:
		Token.objects.create(user=instance)


class ProductCategory(models.Model):
	name = models.CharField(_("Name"), max_length=100, default='')
	slug = models.SlugField(blank=False)
	description = models.TextField(default='', blank=True)
	image = models.ImageField(_("Category Image"), upload_to="cats", blank=True)

	def __str__(self):
		return self.name


class Product(models.Model):
	category = models.ForeignKey(ProductCategory, verbose_name=_("Product Category"), on_delete=models.SET_NULL, null=True)
	title = models.CharField(_("Title"), max_length=100, default='')
	slug = models.SlugField(blank=False)
	description = models.TextField(default='')
	material = models.TextField(default='', blank=True)
	finishing = models.TextField(default='', blank=True)
	unit = models.IntegerField(_("Unit"), default=1)
	unit_price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
	delivery_days = models.CharField(_("Delivery Days"), max_length=100, default='')
	allowed_quantities = models.JSONField(_("Allowed Quantities"), blank=True, null=True)
	additional_fields = models.JSONField(_("Additional Fields"), blank=True, null=True)
	modified = models.DateTimeField(auto_now=True)
	date = models.DateTimeField(auto_now_add=True)

	# For SEO Purpose
	meta_title = models.CharField(_("Meta Title"), max_length=100, default='', blank=True)
	meta_description = models.TextField(_("Meta Description"), default='', blank=True)
	meta_keywords = models.TextField(_("Meta Description"), default='', blank=True)

	def __str__(self):
		return self.title


class ProductGallery(models.Model):
	prod = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name=_("Product"))
	image1 = models.ImageField(_("Image"), upload_to="products", null=True)
	image2 = models.ImageField(_("Image"), upload_to="products", null=True)
	image3 = models.ImageField(_("Image"), upload_to="products", null=True)
	image4 = models.ImageField(_("Image"), upload_to="products", null=True)

	def __str__(self):
		return self.prod.title
